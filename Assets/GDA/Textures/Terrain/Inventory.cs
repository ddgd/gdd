﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
//using UnityEngine.Cursor.visible;

public class Inventory : MonoBehaviour
{
    List<Item> list;
   public GameObject inventory;
    public GameObject container;
    // Use this for initialization
    void Start ()
    {
        Debug.Log("Loh");
        list = new List<Item>();
	
	}
	
	// Update is called once per frame
	void Update () {
	if(Input.GetKeyUp(KeyCode.E))
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            if(Physics.Raycast(ray, out hit))
            {
                Item item = hit.collider.GetComponent<Item>();
                if(item != null)
                {
                    list.Add(item);
                    Destroy(hit.collider.gameObject);
                }
           
            }
        }
        if (Input.GetKeyUp(KeyCode.I))
        {
            Debug.Log("Petuh");
            if (inventory.activeSelf)
            {
                inventory.SetActive(false);
                Debug.Log("Close");
                for(int i = 0; i < inventory.transform.childCount; i++)
                {
                    if(inventory.transform.GetChild(i).transform.childCount > 0)
                    {
                        Destroy(inventory.transform.GetChild(i).transform.GetChild(0).gameObject);
                    }
                }
            }
            else
            {
                inventory.SetActive(true);
                Debug.Log("Open");
                
                int count = list.Count;
                for (int i = 0; i < count; i++)
                {
                    Item it = list[i];
                    if(inventory.transform.childCount >= i)
                    {

                        GameObject img = Instantiate(container);
                        img.transform.SetParent(inventory.transform.GetChild(i).transform);
                        img.GetComponent<Image>().sprite = Resources.Load<Sprite>(it.sprite);
                        img.AddComponent<Button>().onClick.AddListener(() => remove(it, img));
                    }
                   else break;
                }
            }
        }
       
	}
    void remove(Item it, GameObject obj){
            GameObject newo = Instantiate<GameObject>(Resources.Load<GameObject>(it.prefab));
        newo.transform.position = transform.position + transform.forward + transform.up;
        Destroy(obj);
        list.Remove(it);
        }
}
//Надо бы дописать:
//Извлечение
//Перетаскивание